export class Provincia {
  CODPROV: number;
  NOMBRE_PROVINCIA: string;
  COMUNIDAD_CIUDAD_AUTONOMA: string;
  CAPITAL_PROVINCIA: string;
}
