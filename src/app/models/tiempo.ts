export class Tiempo {

  constructor(fecha: string, temperatura: Temperatura, sens_termica: Temperatura, humedad_relativa: Temperatura) {
    this.fecha = fecha;
    this. temperatura = temperatura;
    this. sens_termica = sens_termica;
    this. humedad_relativa = humedad_relativa;
  }

  fecha: string;
  temperatura: Temperatura;
  sens_termica: Temperatura;
  humedad_relativa: Temperatura;
}

export class Temperatura {

  constructor(maxima: number, minima: number) {
    this.maxima = maxima;
    this.minima = minima;
  }

  maxima: number;
  minima: number;
}
