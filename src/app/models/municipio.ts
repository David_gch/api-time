export class Municipio {

  constructor(COD_GEO: number, CODPROV: number) {
    this.COD_GEO = COD_GEO;
    this.CODPROV = CODPROV;
  }

  COD_GEO: number;
  CODPROV: number;
  NOMBRE: string;
}
