import { NgModule } from '@angular/core';

import { AppRoutingModule } from '../../app-routing.module';
import { SharedModule } from '../../shared/shared.module';

import { TimeComponent } from './time.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    AppRoutingModule,
    SharedModule,
    CommonModule
  ],
  exports: [TimeComponent],
  declarations: [TimeComponent]
})
export class TimeModule { }
