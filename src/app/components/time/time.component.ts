import { Component, OnInit, Input } from '@angular/core';
import { Tiempo } from '../../models/tiempo';
import { GeneralService } from '../../services/general.service';

@Component({
  selector: 'app-time',
  templateUrl: './time.component.html',
  styleUrls: ['./time.component.css']
})
export class TimeComponent implements OnInit {

  @Input() tiempo: Tiempo;

  constructor(private generalService: GeneralService) { }

  ngOnInit() {
  }
}
