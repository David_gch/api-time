import { Component, OnInit } from '@angular/core';
import { GeneralService } from '../../services/general.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  type = environment.type;

  constructor(public generalService: GeneralService) { }

  ngOnInit() {
  }

}
