import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppRoutingModule } from '../../app-routing.module';
import { SharedModule } from '../../shared/shared.module';

import { NavComponent } from './nav.component';

@NgModule({
  imports: [
    AppRoutingModule,
    SharedModule,
    CommonModule
  ],
  exports: [NavComponent],
  declarations: [NavComponent]
})
export class NavModule { }
