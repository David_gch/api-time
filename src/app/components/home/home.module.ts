import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppRoutingModule } from '../../app-routing.module';
import { SharedModule } from '../../shared/shared.module';

import { HomeComponent } from './home.component';
import { TimeModule } from '../time/time.module';

@NgModule({
  imports: [
    AppRoutingModule,
    SharedModule,
    CommonModule,
    TimeModule
  ],
  exports: [HomeComponent],
  declarations: [HomeComponent]
})
export class HomeModule { }
