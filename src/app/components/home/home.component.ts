import { Component, OnInit } from '@angular/core';

import { Provincia } from '../../models/provincia';
import { ProvinciaService } from '../../services/provincia.service';

import { Municipio } from '../../models/municipio';
import { MunicipioService } from '../../services/municipio.service';

import { Tiempo } from '../../models/tiempo';
import { TimeService } from '../../services/time.service';
import { GeneralService } from '../../services/general.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  provincias: Provincia[] = [];
  municipios: Municipio[] = [];
  tiempos: Tiempo[] = [];
  provinciaElegida: number = undefined;

  constructor(private provinciaService: ProvinciaService, private municipioService: MunicipioService, private timeService: TimeService
    , private generalService: GeneralService) { }

  ngOnInit() {
    setTimeout(() => this.getProvincias());
  }

  getProvincias(): void {
    this.provinciaService.getProvincias().subscribe(
      provincias => this.provincias = provincias
    );
  }


  selectedProvincia(provincia) {
    this.provinciaElegida = provincia;

    this.municipioService.getMunicipios(provincia).subscribe(
      municipios => this.municipios = Object.values(municipios)
    );
  }

  selectedMunicipio(municipioElegido) {
    const municipio = new Municipio(municipioElegido, this.provinciaElegida);
    this.tiempos = this.timeService.getData(municipio);
  }

}
