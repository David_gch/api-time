import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Municipio } from '../models/municipio';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class MunicipioService {
  // private TimeURL = 'https://www.el-tiempo.net/api/json/v1/provincias';
  private server = environment.baseUrl;
  private TimeURL = '/provincias';
  municipio$: Observable<Municipio[]>;
  municipios: Municipio[] = [];

  constructor(private http: HttpClient) { }

  getMunicipios(idProvincia: number): Observable<Municipio[]> {
    this.municipio$ = this.http.get<Municipio[]>(this.server + this.TimeURL + '/' + idProvincia + '/municipios');
    return this.municipio$;
  }
}
