import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Provincia } from '../models/provincia';
import { environment } from '../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ProvinciaService {
  private server = environment.baseUrl;
  private TimeURL = '/provincias';
  provincia$: Observable<Provincia[]>;

  constructor(private http: HttpClient) {
    this.getData();
  }
  getData() {
    this.provincia$ = this.http.get<Provincia[]>(this.server + this.TimeURL);
  }

  getProvincias(): Observable<Provincia[]> {
    return this.provincia$;
  }
}
