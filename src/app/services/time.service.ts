import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Tiempo, Temperatura } from '../models/tiempo';
import { Municipio } from '../models/municipio';
import { environment } from '../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class TimeService {
  private server = environment.baseUrl;
  private TimeURL = '/provincias';
  observableObject: Observable<Object>;
  arrayObject: Object[] = [];
  object: Object;

  tiempos: Tiempo[] = [];
  temperatura: Temperatura;

  constructor(private http: HttpClient) { }

  getData(municipio: Municipio): Tiempo[] {
    this.tiempos = [];
    this.observableObject = this.http.get(this.server + this.TimeURL + '/' +
     municipio.CODPROV + '/municipios/' +  municipio.COD_GEO + '/weather');

    this.observableObject.subscribe(
      data => {
        this.arrayObject = data['prediccion']['dia'];
        for (let i = 0; i < 3; i++) {
          const tiempo = new Tiempo(
            this.arrayObject[i]['@attributes']['fecha'],
            new Temperatura(this.arrayObject[i]['temperatura']['maxima'],
              this.arrayObject[i]['temperatura']['minima']),
            new Temperatura(this.arrayObject[i]['sens_termica']['maxima'],
            this.arrayObject[i]['sens_termica']['minima']),
            new Temperatura(this.arrayObject[i]['humedad_relativa']['maxima'],
            this.arrayObject[i]['humedad_relativa']['minima'])
            );
          this.tiempos.push(tiempo);
        }
      }
    );
    return this.tiempos;
  }

}
