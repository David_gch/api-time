export const environment = {
  production: true,
  type: 'Prod',
  baseUrl: 'https://www.el-tiempo.net/api/json/v1'
};
